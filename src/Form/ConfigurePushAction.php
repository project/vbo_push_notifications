<?php

namespace Drupal\vbo_push_notifications\Form;

use Drupal\Core\State\State;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Form\FormBase;
use Drupal\views_bulk_operations\Form\ViewsBulkOperationsFormTrait;
use Drupal\views_bulk_operations\Service\ViewsBulkOperationsActionManager;
use Drupal\views_bulk_operations\Service\ViewsBulkOperationsActionProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Push Action Class.
 */
class ConfigurePushAction extends FormBase {

  use ViewsBulkOperationsFormTrait;

  /**
   * Entity type Manager Service.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Views Bulk Operations action manager.
   *
   * @var \Drupal\views_bulk_operations\Service\ViewsBulkOperationsActionManager
   */
  protected $actionManager;

  /**
   * Views Bulk Operations action processor.
   *
   * @var \Drupal\views_bulk_operations\Service\ViewsBulkOperationsActionProcessorInterface
   */
  protected $actionProcessor;

  /**
   * State service.
   *
   * @var Drupal\Core\State\State
   */
  public $state;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   User private temporary storage factory.
   * @param \Drupal\views_bulk_operations\Service\ViewsBulkOperationsActionManager $action_manager
   *   Extended action manager object.
   * @param \Drupal\views_bulk_operations\Service\ViewsBulkOperationsActionProcessor $action_processor
   *   Views Bulk Operations action processor.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param Drupal\Core\State\State $state
   *   State Service.
   */
  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    ViewsBulkOperationsActionManager $action_manager,
    ViewsBulkOperationsActionProcessor $action_processor,
    EntityTypeManagerInterface $entity_type_manager,
    State $state
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->tempStoreFactory = $temp_store_factory;
    $this->actionManager = $action_manager;
    $this->actionProcessor = $action_processor;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('plugin.manager.views_bulk_operations_action'),
      $container->get('views_bulk_operations.processor'),
      $container->get('entity_type.manager'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vbo_push_notifications_confirm_action';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $view_id = NULL, $display_id = NULL) {

    $form_data = $this->getFormData($view_id, $display_id);

    // TODO: display an error msg, redirect back.
    if (!isset($form_data['action_id'])) {
      return;
    }

    // Get views display's relationship info to generate token types.
    /** @var \Drupal\views\ViewEntityInterface $view */
    $view = $this->entityTypeManager->getStorage('view')->load($view_id);
    $view_executatble = $view->getExecutable();
    $view_executatble->setDisplay($display_id);
    $relationships = $view_executatble->display_handler->getHandlers('relationship');

    // Get base entity type of the view.
    $entity_type = $view_executatble->getBaseEntityType()->id();
    $relationship_token_types[$entity_type] = $entity_type;

    foreach ($relationships as $relationship) {
      if (array_key_exists('entity type', $relationship->definition)) {
        $entity_type = $relationship->definition['entity type'];
        $relationship_token_types[$entity_type] = $entity_type;
      }
    }

    $state_value = $form_state->get('vbo_push_notifications');

    $form['push_notification'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Push Notification'),
      '#description' => $this->t('Enter message'),
      '#default_value' => isset($state_value['push_notification']) ? $state_value['push_notification'] : NULL,
      '#rows' => 3,
    ];
    $form['remember_push_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remember Push Notification'),
      '#default_value' => isset($state_value['remember_sms_message']) ? $state_value['remember_push_notification'] : TRUE,
    ];
    $form['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#show_restricted' => TRUE,
      '#token_types' => $relationship_token_types,
      '#weight' => 90,
    ];

    $form['#title'] = $this->t('Send Push Notification');

    $actions = $form['actions'];
    unset($form['actions']);
    $form['actions'] = $actions;

    $form['list'] = $this->getListRenderable($form_data);

    // :D Make sure the submit button is at the bottom of the form
    // and is editale from the action buildConfigurationForm method.
    $form['actions']['#weight'] = 666;
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#submit' => [
        [$this, 'submitForm'],
      ],
    ];
    $this->addCancelButton($form);

    $action = $this->actionManager->createInstance($form_data['action_id']);

    if (method_exists($action, 'setContext')) {
      $action->setContext($form_data);
    }

    $form_state->set('vbo_push_notifications', $form_data);
    $form = $action->buildConfigurationForm($form, $form_state);

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $view_data = $form_state->getStorage();
    $form_state->setRedirectUrl($view_data['redirect_url']);

    $push_notification = $form_state->getValue('push_notification');
    $remember_push_notification = $form_state->getValue('remember_push_notification');

    if ($remember_push_notification) {
      $state_value = [
        'remember_push_notification' => $remember_push_notification,
        'push_notification' => $push_notification,
      ];
    }
    else {
      $state_value = [
        'remember_push_notification' => $remember_push_notification,
      ];
    }

    $this->state->set('vbo_push_notifications', $state_value);

    // Pass the sms_message content to the action processor.
    $view_data['push_notification'] = $push_notification;

    $this->actionProcessor->executeProcessing($view_data);
    $this->tempStoreFactory->get($view_data['tempstore_name'])->delete($this->currentUser()->id());
  }

}
