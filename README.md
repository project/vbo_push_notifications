CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Allows sending customized Push Notifications via Firebase, with all the
available flexibility of Views Bulk Operations (VBO) and the Token module.

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/vbo_push_notifications).

REQUIREMENTS
------------

This module requires Views Bulk Operations, Firebase Push Notification (FCM)
and Token modules outside of Drupal core.


INSTALLATION
------------

 * Install the VBO Push Notifications module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420
   for further information.

CONFIGURATION
-------------

1. [Install as usual](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)
2. Create a view having the field storing the device registration token
3. Add the field “Global: Views bulk operations”
4. Select the ‘Send Push Notification’ action
5. Choose the field used for storing a device's registration token
6. Configure the view as needed and save it
7. Run the view and choose where to send the Push Notifications


MAINTAINERS
-----------

Current maintainers:

 * Jose Fernandes (introfini) - https://www.drupal.org/u/introfini

Supporting organization:

[Bloomidea](https://bloomidea.com/en)
